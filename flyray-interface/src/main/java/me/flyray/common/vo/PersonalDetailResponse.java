package me.flyray.common.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import java.io.Serializable;

/**
 * 查询用户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "客户详情查询参数响应")
public class PersonalDetailResponse implements Serializable {

	private String personalId;

	//手机号
	private String phone;

	private String email;

	//用户名称
	private String realName;

	//性别 1：男 2：女
	private String sex;

	//认证状态  00：未认证  02：认证成功
	private String authenticationStatus;

	// 归属人id（存后台管理系统登录人员id）指所属客户经理id
	private String ownerId;

	// 归属人名称（存后台管理系统登录人员名称）所属哪个客户经理
	private String ownerName;

	//推荐人树
	private String referrerNameTree;

	//本年度积分余额
	private String currentPointsBalance;

	//历年积分余额
	private String bygonePointsBalance;

	//客户等级
	private String personalLevel;

	// 备注
	private String remark;

	//凭据 第三方唯一标识如：微信openid
	private String credential;

}
