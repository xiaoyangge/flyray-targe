package me.flyray.common.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 查询用户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@ApiModel(value = "邀请客户查询参数响应")
public class PersonalInviteResponse implements Serializable {

	private String personalId;

	//手机号
	private String phone;

	//用户名称
	private String realName;

	private String createTime;

}
