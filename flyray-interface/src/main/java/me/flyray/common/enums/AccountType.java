package me.flyray.common.enums;

/**
 * @Author: bolei
 * @date: 9:14 2019/1/8
 * @Description: 平台支持账户类型于字段表一致
 * 账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，
 * ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，
 * ACC009：平台服务费账户 ACC010：火源账户 ACC011：火钻账户 ACC012：原力值账户
 */

public enum AccountType {

    BALANCE("ACC001","余额账户"),
    RED("ACC002","红包账户"),
    POINT("ACC003","积分账户"),
    FEE("ACC004","手续费账户"),
    SETTLE("ACC005","已结算账户"),
    TX_TOTAL("ACC006","总交易金额账户"),
    FREEZE("ACC007","冻结金额账户"),
    PLATFORM_FEE("ACC008","平台管理费账户"),
    PLATFORM_SERVICE_FEE("ACC009","平台服务费账户"),
    FIRE_SOURCE("ACC010","火源账户"),
    FIRE_DRILL("ACC011","火钻账户"),
    FORCE("ACC012","原力值账户");

    private String code;
    private String desc;

    private AccountType (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static AccountType getAccountType(String code){
        for(AccountType o : AccountType.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }

}
