package me.flyray.crm.facade.request.personal;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author: bolei
 * @date: 15:50 2019/2/23
 * @Description: 第三方授权注册请求
 */

@Data
public class Personal3rdAuthRegisterRequest implements Serializable {

    /**
     * 平台编号
     */
    @NotNull(message="平台编号不能为空")
    private String platformId;

    /**
     * 第三方平台编号
     *  weixin alipay
     */
    @NotNull(message="第三方平台编号不能为空")
    private String thirdCode;

    /**
     * 第三方ID
     */
    @NotNull(message="第三方ID不能为空")
    private String XId;

    /**
     * 第三方昵称
     */
    private String nickname;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 验证码
     */
    private String verificationCode;

}
