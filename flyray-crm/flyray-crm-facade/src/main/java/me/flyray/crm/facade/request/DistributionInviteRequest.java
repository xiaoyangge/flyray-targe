package me.flyray.crm.facade.request;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Author: bolei
 * @date: 20:49 2019/2/15
 * @Description: 要求请求参数
 */

@Data
public class DistributionInviteRequest {

    /**
     * 邀请人编号
     */
    private String inviterPersonalId;

    /**
     * 邀请人openid
     */
    private String inviterOpenId;

    /**
     * 被邀请人personalId
     */
    private String inviteePersonalId;

    /**
     * 被邀请人openid
     */
    private String inviteeOpenId;

    /**
     * 被邀请人phone
     */
    private String inviteePhone;

    /**
     * 被邀请人姓名
     */
    private String inviteeRealName;

    /**
     * 被邀请人性别
     */
    private String inviteeSex;

    /**
     * 被邀请人email
     */
    private String inviteeEmail;

}
