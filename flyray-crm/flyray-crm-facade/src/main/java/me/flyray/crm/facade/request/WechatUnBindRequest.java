package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 微信解绑
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:24:07
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "微信解绑")
public class WechatUnBindRequest implements Serializable {

    @NotNull(message="参数[personalId]不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

}
