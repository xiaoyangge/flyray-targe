package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询平台列表请求参数")
public class PlatformBaseRequest implements Serializable {
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	/**
	 * 用户类型1，系统管理员2，平台管理员3，商户管理员4、平台操作员
	 */
	@ApiModelProperty(value = "用户类型")
	private int userType;
}
