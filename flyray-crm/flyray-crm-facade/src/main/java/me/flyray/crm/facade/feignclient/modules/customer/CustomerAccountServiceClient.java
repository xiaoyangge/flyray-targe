package me.flyray.crm.facade.feignclient.modules.customer;

import io.swagger.annotations.ApiOperation;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.*;
import me.flyray.crm.facade.response.AccountJournalQueryResponse;
import me.flyray.crm.facade.response.CustomerAccountQueryResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 21:38 2019/2/28
 * @Description: 类描述
 */

@FeignClient(value = "flyray-crm-core")
public interface CustomerAccountServiceClient {

    /**
     * @param
     * @des  根据交易类型增加总交易账户
     * @return
     */
    @RequestMapping(value = "feign/customer/intoAccount",method = RequestMethod.POST)
    BaseApiResponse<Void> intoAccount(@RequestBody @Valid IntoAccountRequest intoAccountRequest);

    /**
     * 个人或者商户直接出账的接口
     * */
    @RequestMapping(value = "feign/customer/outAccount",method = RequestMethod.POST)
    BaseApiResponse<Void> outAccount(@RequestBody @Valid OutAccountRequest outAccountRequest);

    /***
     * 个人或者商户解冻并出账的接口
     * */
    @RequestMapping(value = "feign/customer/unfreezeAndOutAccount",method = RequestMethod.POST)
    BaseApiResponse<Void> unfreezeAndOutAccount(@RequestBody @Valid UnfreezeAndOutAccountRequest unFreezeAndOutAccountRequest);

    /**
     * 个人或者商户转账接口
     * @return
     */
    @ApiOperation("个人或者商户转账相关的接口")
    @RequestMapping(value = "feign/customer/accounttransfer",method = RequestMethod.POST)
    Map<String, Object> accountTransfer(@RequestBody @Valid AccountTransferRequest accountTransferRequest);

    /***
     * 个人或者商户解冻并转账接口
     * 适用于提现
     * */
    @RequestMapping(value = "feign/customer/unfreezeAndTransfer",method = RequestMethod.POST)
    BaseApiResponse<Void> unfreezeAndTransfer(@RequestBody @Valid UnfreezeAndTransferRequest unFreezeAndTransferRequest);

    /**
     * 账户查询
     * @param
     * @return withdrawApply
     */
    @RequestMapping(value = "feign/customer/accountQuery",method = RequestMethod.POST)
    BaseApiResponse<List<CustomerAccountQueryResponse>> accountQuery(@RequestBody @Valid AccountQueryRequest accountQueryRequest);

    /**
     * 查询账户交易流水
     * @param accountJournalQueryRequest
     * @return
     */
    @RequestMapping(value = "feign/customer/accountJournalQuery",method = RequestMethod.POST)
    BaseApiResponse<List<AccountJournalQueryResponse>> accountJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest);

    /**
     * 查询账户详细交易流水
     * 包含了账户对应的客户信息
     * @param accountJournalQueryRequest
     * @return
     */
    @ApiOperation("查询账户详细交易流水")
    @RequestMapping(value = "feign/customer/accountDetailJournalQuery",method = RequestMethod.POST)
    @ResponseBody
    BaseApiResponse<List<AccountJournalQueryResponse>> accountDetailJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest);

}
