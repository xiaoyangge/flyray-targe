package me.flyray.crm.facade.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * @Author: bolei
 * @date: 16:14 2019/2/13
 * @Description: 判断用户是否是管理员
 */

public enum IsAdmin {

    /**
     * 非admin用户
     */
    ADMIN_USER("0", "非admin用户"),
    /**
     * admin用户
     */
    NON_ADMIN_USER("1", "admin用户");

    private String code;

    private String desc;

    IsAdmin(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    /**
     * Json 枚举序列化
     */
    @JsonCreator
    public static IsAdmin getInstanceById(Integer id) {
        if (id == null) {
            return null;
        }
        for (IsAdmin status : values()) {
            if (id.equals(status.getCode())) {
                return status;
            }
        }
        return null;
    }

}
