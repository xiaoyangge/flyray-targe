package me.flyray.crm.facade.response;

import java.io.Serializable;

/**
 * 返回公共参数
 * 
 */
public class BaseResponse implements Serializable {

	private String code;// 返回错误码
	private String message;// 返回错误信息
	private String resJrnNo;// 返回响应流水号
	private String reqJrnNo;// 返回请求流水号
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResJrnNo() {
		return resJrnNo;
	}

	public void setResJrnNo(String resJrnNo) {
		this.resJrnNo = resJrnNo;
	}

	public String getReqJrnNo() {
		return reqJrnNo;
	}

	public void setReqJrnNo(String reqJrnNo) {
		this.reqJrnNo = reqJrnNo;
	}

	public BaseResponse(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public BaseResponse() {
		super();
	}

	
}
