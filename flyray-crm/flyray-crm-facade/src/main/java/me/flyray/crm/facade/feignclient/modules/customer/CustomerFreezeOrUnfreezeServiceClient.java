package me.flyray.crm.facade.feignclient.modules.customer;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.facade.request.FreezeRequest;
import me.flyray.crm.facade.request.UnfreezeRequest;
import me.flyray.crm.facade.response.FreezeJournalResponse;
import me.flyray.crm.facade.response.UnfreezeJournalResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @Author: bolei
 * @date: 21:36 2019/2/28
 * @Description: 类描述
 */

@FeignClient(value = "flyray-crm-core")
public interface CustomerFreezeOrUnfreezeServiceClient {

    /***
     * 个人或者商户冻结
     * */
    @RequestMapping(value = "/freeze",method = RequestMethod.POST)
    public BaseApiResponse<FreezeJournalResponse> freeze(@RequestBody @Valid FreezeRequest freezeRequest);

    /**
     * 个人或者商户解冻
     * */
    @RequestMapping(value = "/unfreeze",method = RequestMethod.POST)
    public BaseApiResponse<UnfreezeJournalResponse> unfreeze(@RequestBody @Valid UnfreezeRequest unFreezeRequest);

}
