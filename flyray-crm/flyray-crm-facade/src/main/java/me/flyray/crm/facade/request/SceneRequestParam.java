package me.flyray.crm.facade.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 平台/商户支付方式
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "场景请求参数")
public class SceneRequestParam implements Serializable {
	
	@ApiModelProperty(value = "序号")
	private String id;
	
	@NotNull(message="场景编号不能为空")
	@ApiModelProperty(value = "场景编号")
	private String sceneCode;

	@ApiModelProperty(value = "场景名称")
    private String sceneName;
	
}
