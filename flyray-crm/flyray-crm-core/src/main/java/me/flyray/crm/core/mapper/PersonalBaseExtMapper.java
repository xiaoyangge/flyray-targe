package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PersonalBaseExt;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 个人基础信息扩展表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-14 14:15:09
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBaseExtMapper extends Mapper<PersonalBaseExt> {

    Integer getPageCount(@Param("personalId")String personalId, @Param("realName")String realName, @Param("phone")String phone,
                         @Param("authenticationStatus")String authenticationStatus, @Param("ownerId")String ownerId,
                         @Param("personalLevel")String personalLevel);

    List<PersonalBase> getPageInfos(@Param("personalId")String personalId, @Param("realName")String realName, @Param("phone")String phone,
                                    @Param("authenticationStatus")String authenticationStatus, @Param("ownerId")String ownerId,
                                    @Param("personalLevel")String personalLevel, @Param("startRow")Integer startRow, @Param("pageSize")Integer pageSize);

    Integer updateLevelByPersonalId(@Param("personalId")String personalId, @Param("personalLevel")String personalLevel);

    PersonalBaseExt queryExtDetailByPersonalId(@Param("personalId") String personalId);
}
