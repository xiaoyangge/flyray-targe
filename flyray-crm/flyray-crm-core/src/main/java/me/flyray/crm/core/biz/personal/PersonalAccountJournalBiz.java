package me.flyray.crm.core.biz.personal;

import me.flyray.crm.core.entity.PersonalAccountJournal;
import me.flyray.crm.core.mapper.PersonalAccountJournalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 个人账户流水（充、转、提、退、冻结流水）
 *
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PersonalAccountJournalBiz extends BaseBiz<PersonalAccountJournalMapper, PersonalAccountJournal> {
	
	private static final Logger logger= LoggerFactory.getLogger(PersonalAccountJournalBiz.class);

}

