package me.flyray.crm.core.api;

import javax.validation.Valid;

import me.flyray.crm.facade.request.PersonalGjjInfoReq;
import me.flyray.crm.facade.response.BaseResponse;
import me.flyray.crm.facade.response.PersonalGjjInfoRsp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.personal.PersonalGjjInfoBiz;

import io.swagger.annotations.ApiOperation;


/**
 * 公积金账户信息
 * @author Administrator
 *
 */
@Controller
@RequestMapping("personalGjjApi")
public class PersonalGjjInfoController{
	private final static Logger logger = LoggerFactory.getLogger(PersonalGjjInfoController.class);
	@Autowired
	private PersonalGjjInfoBiz personalGjjInfoBiz;
	
	/**
	 * 查询公积金信息
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询公积金信息")
	@RequestMapping(value = "/queryInfo",method = RequestMethod.POST)
    @ResponseBody
    public PersonalGjjInfoRsp openMerchant(@RequestBody @Valid PersonalGjjInfoReq personalGjjInfoReq) throws Exception {
		return  personalGjjInfoBiz.personalGjjQuery(personalGjjInfoReq);
    }
	
	/**
	 * 添加公积金账户信息
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("添加公积金账户信息")
	@RequestMapping(value = "/addInfo",method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse mercustInfoQuery(@RequestBody @Valid PersonalGjjInfoReq personalGjjInfoReq) throws Exception {
		return  personalGjjInfoBiz.insertGjjInfo(personalGjjInfoReq);
    }
	
	/**
	 * 更新公积金账户信息
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("更新公积金账户信息")
	@RequestMapping(value = "/updateInfo",method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse updateMerchantInfo(@RequestBody @Valid PersonalGjjInfoReq personalGjjInfoReq) throws Exception {
		return  personalGjjInfoBiz.updateGjjInfo(personalGjjInfoReq);
    }
	
	
}
