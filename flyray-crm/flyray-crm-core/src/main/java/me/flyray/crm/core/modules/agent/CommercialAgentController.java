package me.flyray.crm.core.modules.agent;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.biz.agent.CommercialAgentBiz;
import me.flyray.crm.facade.request.CommercialAgentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @Author: bolei
 * @date: 10:46 2019/2/14
 * @Description: 类描述
 */

@RestController
@RequestMapping("commercialAgent")
public class CommercialAgentController {

    @Autowired
    private CommercialAgentBiz commercialAgentBiz;
    /**
     * 添加代理商基础信息
     * @time 创建时间:2018年7月16日下午6:02:15
     * @param commercialAgentRequest
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse add(@RequestBody @Valid CommercialAgentRequest commercialAgentRequest){
        return commercialAgentBiz.add(commercialAgentRequest);
    }

}
