package me.flyray.crm.core.mapper;

import me.flyray.crm.core.entity.PlatformAccoutConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * 平台所支持账户配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformAccoutConfigMapper extends Mapper<PlatformAccoutConfig> {
	
}
