package me.flyray.crm.core.biz.agent;

import lombok.extern.slf4j.Slf4j;
import me.flyray.common.constant.UserConstant;
import me.flyray.common.enums.UserType;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.util.EntityUtils;
import me.flyray.common.util.SnowFlake;
import me.flyray.common.vo.admin.AdminUserRequest;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PersonalBaseExt;
import me.flyray.crm.core.mapper.PersonalBaseExtMapper;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.facade.enumeration.IsAdmin;
import me.flyray.crm.facade.feignclient.modules.admin.AdminUserServiceClient;
import me.flyray.crm.facade.request.CommercialAgentRequest;
import me.flyray.crm.facade.request.PersonalBaseRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 10:47 2019/2/14
 * @Description: 代理商处理逻辑
 */

@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CommercialAgentBiz {

    @Autowired
    private AdminUserServiceClient adminUserServiceClient;
    @Autowired
    private PersonalBaseMapper personalBaseMapper;
    @Autowired
    private PersonalBaseExtMapper personalBaseExtMapper;

    /**
     * 添加代理商
     * @date: 10:47 2019/2/14
     * @param personalBaseRequest
     * @return
     */
    public BaseApiResponse add(CommercialAgentRequest commercialAgentRequest){
        //添加admin登陆用户及赋予角色
        AdminUserRequest adminUserRequest = new AdminUserRequest();
        adminUserRequest.setPlatformId(commercialAgentRequest.getPlatformId());
        adminUserRequest.setDeptId(Integer.valueOf(commercialAgentRequest.getDeptId()));
        adminUserRequest.setDeptName(commercialAgentRequest.getDeptName());
        adminUserRequest.setUsername(commercialAgentRequest.getUsername());

        adminUserRequest.setUserType(UserType.AGENT_OPERATOR.getCode());
        adminUserServiceClient.addUser(adminUserRequest);
        log.info("【添加代理商基础信息】   请求参数：{}",EntityUtils.beanToMap(commercialAgentRequest));
        Map<String, Object> respMap = new HashMap<String, Object>();
        PersonalBase personalBase = new PersonalBase();
        PersonalBaseExt personalBaseExt = new PersonalBaseExt();
        BeanUtils.copyProperties(commercialAgentRequest,personalBase);
        BeanUtils.copyProperties(commercialAgentRequest,personalBaseExt);
        String personalId = String.valueOf(SnowFlake.getId());
        personalBase.setPersonalId(personalId);
        personalBase.setAuthenticationStatus("00");
        personalBase.setStatus("00");
        personalBaseMapper.insert(personalBase);
        personalBaseExt.setPersonalId(personalId);
        personalBaseExtMapper.insert(personalBaseExt);
        return BaseApiResponse.newSuccess();
    }

}
