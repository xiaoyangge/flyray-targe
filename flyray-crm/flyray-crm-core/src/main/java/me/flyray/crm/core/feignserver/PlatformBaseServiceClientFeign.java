package me.flyray.crm.core.feignserver;

import java.util.HashMap;
import java.util.Map;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.common.util.EntityUtils;
import me.flyray.crm.core.entity.PlatformSafetyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.platform.PlatformSafetyConfigBiz;

@Controller
@RequestMapping("feign/platform")
public class PlatformBaseServiceClientFeign {
	
	@Autowired
	private PlatformSafetyConfigBiz platformSafetyConfigBiz;

	@RequestMapping(value = "queryPlatformInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryPlatformInfo(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		PlatformSafetyConfig platformSafetyConfigReq = new PlatformSafetyConfig();
    		platformSafetyConfigReq.setAppId((String) param.get("appid"));
    		PlatformSafetyConfig platformSafetyConfig = platformSafetyConfigBiz.selectOne(platformSafetyConfigReq);
//    		PlatformSafetyConfigCommon platformSafetyConfigCommon = new PlatformSafetyConfigCommon();
//    		BeanUtils.copyProperties(platformSafetyConfig, platformSafetyConfigCommon);
    		Map<String, Object> platformSafetyConfigCommon = EntityUtils.beanToMap(platformSafetyConfig);
    		
    		if(null != platformSafetyConfig){
    			result.put("platformSafetyConfig", platformSafetyConfigCommon);
    			result.put("code", BizResponseCode.OK.getCode());
    			result.put("message", BizResponseCode.OK.getMessage());
    			result.put("success", true);
    		}else{
    			result.put("code", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getCode());
    			result.put("message", BizResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getMessage());
    			result.put("success", false);
    		}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			result.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			result.put("success", false);
		}
    	return result;
    }
	
	
}
