package me.flyray.crm.core.biz.personal;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.util.SnowFlake;
import me.flyray.common.vo.*;
import me.flyray.crm.core.entity.CustomerBaseAuth;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.core.entity.PersonalBaseExt;
import me.flyray.crm.core.entity.PersonalDistributionRelation;
import me.flyray.crm.core.mapper.CustomerBaseAuthMapper;
import me.flyray.crm.core.mapper.PersonalBaseExtMapper;
import me.flyray.crm.core.mapper.PersonalBaseMapper;
import me.flyray.crm.core.mapper.PersonalDistributionRelationMapper;
import me.flyray.crm.core.util.PageResult;
import me.flyray.crm.facade.feignclient.modules.admin.AdminUserServiceClient;
import me.flyray.crm.facade.request.PersonalBaseAddRequest;
import me.flyray.crm.facade.request.PersonalBaseModifyRequest;
import me.flyray.crm.facade.request.WechatUnBindRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;
import org.springframework.util.CollectionUtils;
import sun.font.TrueTypeFont;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 个人基础信息扩展表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-14 14:15:09
 */
@Service
public class PersonalBaseExtBiz {

    @Resource
    private PersonalBaseExtMapper personalBaseExtMapper;
    @Resource
    private PersonalBaseMapper personalBaseMapper;
    @Resource
    private PersonalDistributionRelationMapper personalDistributionRelationMapper;
    @Resource
    private AdminUserServiceClient adminUserServiceClient;
    @Resource
    private CustomerBaseAuthMapper customerBaseAuthMapper;

    public List<QueryPersonalInfoExtResponse> queryWithPointsList(QueryPersonalBaseExtRequest request, PageResult pageResult) {
        Integer pageSize = request.getPageSize();
        Integer currentPage = request.getCurrentPage();
        if(pageSize == null || pageSize.intValue() == 0){
            pageSize = 10;
        }
        if(currentPage == null || currentPage.intValue() == 0){
            currentPage = 1;
        }
        Integer startRow = (currentPage -1) * pageSize;
        if("1".equals(request.getUserType())){
            request.setOwnerId(request.getCustomerId());
        } else{
            request.setOwnerId(request.getUserId());
        }
        Integer count  = personalBaseExtMapper.getPageCount(request.getPersonalId(), request.getRealName(), request.getMobile(),
                request.getAuthenticationStatus(), request.getOwnerId(), request.getPersonalLevel());
        List<PersonalBase> personalBaseExtList = personalBaseExtMapper.getPageInfos(request.getPersonalId(), request.getRealName(), request.getMobile(),
                request.getAuthenticationStatus(), request.getOwnerId(), request.getPersonalLevel(), startRow, pageSize);
        List<QueryPersonalInfoExtResponse> queryPersonalInfoExtResponseList = new ArrayList<QueryPersonalInfoExtResponse>();
        if(!CollectionUtils.isEmpty(personalBaseExtList)){
            for (PersonalBase personalBase :personalBaseExtList) {
                QueryPersonalInfoExtResponse queryPersonalInfoExtResponse = buildQueryPersonalInfoExtResponse(personalBase);
                queryPersonalInfoExtResponseList.add(queryPersonalInfoExtResponse);
            }
        }
        Integer pageCount = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;
        pageResult.setCurrentPage(currentPage);
        pageResult.setPageCount(pageCount);
        pageResult.setRowCount(count);
        pageResult.setPageSize(pageSize);
        return queryPersonalInfoExtResponseList;
    }


    public BaseApiResponse updatePersonalLevels(PersonalLevelRequest request) {
        for(String personalId : request.getPersonalIds()) {
            personalBaseExtMapper.updateLevelByPersonalId(personalId, request.getPersonalLevel());
        }
        return BaseApiResponse.newSuccess(Boolean.TRUE);
    }

    private QueryPersonalInfoExtResponse buildQueryPersonalInfoExtResponse(PersonalBase personalBase) {
        QueryPersonalInfoExtResponse queryInfo = new QueryPersonalInfoExtResponse();
        queryInfo.setId(personalBase.getId());
        queryInfo.setPhone(personalBase.getPhone());
        queryInfo.setRealName(personalBase.getRealName());
        queryInfo.setOwnerId(personalBase.getOwnerId());
        queryInfo.setOwnerName(personalBase.getOwnerName());
        SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        queryInfo.setUpdateTime(sdf.format(personalBase.getUpdateTime()));
        queryInfo.setPersonalLevel(personalBase.getParentLevel());
        queryInfo.setSex("男");
        if("2".equals(personalBase.getSex())){
            queryInfo.setSex("女");
        }
        queryInfo.setAuthenticationStatus("未激活");
        if("02".equals(personalBase.getAuthenticationStatus())){
            queryInfo.setAuthenticationStatus("已激活");
        }
        //TODO 积分功能未实现
        queryInfo.setPointsBalance("");
        PersonalDistributionRelation personalDistributionRelation = personalDistributionRelationMapper.queryByPersonalId(queryInfo.getPersonalId());
        if(personalDistributionRelation != null){
            queryInfo.setReferrerId(personalDistributionRelation.getParentId());
            queryInfo.setReferrerName(personalDistributionRelation.getParentName());
        }
        return queryInfo;
    }

    public BaseApiResponse queryPersonalDetail(PersonalDetailRequest request) {
        PersonalBase personalBase = personalBaseMapper.queryDetailByPersonalId(request.getPersonalId() );
        PersonalBaseExt personalBaseExt = personalBaseExtMapper.queryExtDetailByPersonalId(request.getPersonalId() );
        PersonalDetailResponse personalDetailResponse = buildPersonalDetailResponse(personalBase, personalBaseExt);
        return BaseApiResponse.newSuccess(personalDetailResponse);
    }

    private PersonalDetailResponse buildPersonalDetailResponse(PersonalBase personalBase, PersonalBaseExt personalBaseExt) {
        PersonalDetailResponse personalDetailResponse = new PersonalDetailResponse();
        personalDetailResponse.setPersonalId(personalBase.getPersonalId());
        personalDetailResponse.setPhone(personalBase.getPhone());
        personalDetailResponse.setRealName(personalBase.getRealName());
        personalDetailResponse.setEmail(personalBaseExt .getEmail());
        personalDetailResponse.setOwnerId(personalBase.getOwnerId());
        personalDetailResponse.setOwnerName(personalBase.getOwnerName());
        personalDetailResponse.setPersonalLevel(personalBaseExt.getPersonalLevel());
        personalDetailResponse.setRemark(personalBase.getRemark());
        String referrerNameTree = queryReferrerNameTree(personalBase.getPersonalId());
        personalDetailResponse.setReferrerNameTree(referrerNameTree);
        personalDetailResponse.setSex("男");
        if("2".equals(personalBase.getSex())){
            personalDetailResponse.setSex("女");
        }
        personalDetailResponse.setAuthenticationStatus("未激活");
        if("02".equals(personalBase.getAuthenticationStatus())){
            personalDetailResponse.setAuthenticationStatus("已激活");
        }
        //TODO 设置历年积分数
        personalDetailResponse.setBygonePointsBalance("");
        //TODO 设置当前积分数
        personalDetailResponse.setCurrentPointsBalance("");
        CustomerBaseAuth customerBaseAuth = customerBaseAuthMapper.getByPersonalId(personalBase.getPersonalId());
        if(customerBaseAuth != null){
            personalDetailResponse.setCredential(customerBaseAuth.getCredential());
        }
        return personalDetailResponse;
    }

    private String queryReferrerNameTree(String personalId) {
        StringBuffer stringBuffer = new StringBuffer();
        PersonalDistributionRelation personalDistributionRelation = personalDistributionRelationMapper.queryByPersonalId(personalId);
        while (personalDistributionRelation != null){
            stringBuffer.append(personalDistributionRelation.getParentName());
            personalDistributionRelation = personalDistributionRelationMapper.queryByPersonalId(personalDistributionRelation.getPersonalId());
            if(personalDistributionRelation != null){
                stringBuffer.append("—>");
            }
        }
        return stringBuffer.toString();
    }

    public List<PersonalInviteResponse> queryInvitePersonal(PersonalInviteRequest request, PageResult pageResult) {
        Integer pageSize = request.getPageSize();
        Integer currentPage = request.getCurrentPage();
        if(pageSize == null || pageSize.intValue() == 0){
            pageSize = 10;
        }
        if(currentPage == null || currentPage.intValue() == 0){
            currentPage = 1;
        }
        Integer startRow = (currentPage -1) * pageSize;

        Integer count  = personalDistributionRelationMapper.getPageCount(request.getPersonalId());
        List<PersonalDistributionRelation> personalRelationList = personalDistributionRelationMapper.getPageInfos(request.getPersonalId(), startRow, pageSize);
        List<PersonalInviteResponse> personalInviteResponses = new ArrayList<PersonalInviteResponse>();
        if(!CollectionUtils.isEmpty(personalRelationList)){
            for (PersonalDistributionRelation personalRelation :personalRelationList) {
                PersonalInviteResponse personalInviteResponse = buildPersonalInviteResponse(personalRelation);
                personalInviteResponses.add(personalInviteResponse);
            }
        }
        Integer pageCount = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;
        pageResult.setCurrentPage(currentPage);
        pageResult.setPageCount(pageCount);
        pageResult.setRowCount(count);
        pageResult.setPageSize(pageSize);
        return personalInviteResponses;

    }

    private PersonalInviteResponse buildPersonalInviteResponse(PersonalDistributionRelation personalRelation) {
        PersonalInviteResponse personalInviteResponse = new PersonalInviteResponse();
        PersonalBase personalBase = personalBaseMapper.queryDetailByPersonalId(personalRelation.getPersonalId() );
        if(personalBase != null){
            personalInviteResponse.setPersonalId(personalBase.getPersonalId());
            personalInviteResponse.setRealName(personalBase.getRealName());
            personalInviteResponse.setPhone(personalBase.getPhone());
            SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            personalInviteResponse.setCreateTime(sdf.format(personalBase.getCreateTime()));
        }
        return personalInviteResponse;
    }

    public BaseApiResponse updatePersonalOwner(PersonalOwnerRequest request) {
        for(String personalId : request.getPersonalIds()) {
            personalBaseMapper.updateOwnerByPersonalId(personalId, request.getOwnerId(), request.getOwnerName());
        }
        return BaseApiResponse.newSuccess(Boolean.TRUE);
    }

    public BaseApiResponse addPersonal(PersonalBaseAddRequest request,  String customerId, String customerName) {
        PersonalBase personalBase = new PersonalBase();
        String personalId = String.valueOf(SnowFlake.getId());
        personalBase.setRealName(request.getRealName());
        personalBase.setSex(request.getSex());
        personalBase.setAuthenticationStatus("02");
        personalBase.setStatus("00");
//        personalBase.setCustomerId(customerId);
        personalBase.setPersonalId(personalId);
        Date nowDate =  new Date();
        personalBase.setCreateTime(nowDate);
        personalBase.setUpdateTime(nowDate);
//        personalBase.setOwnerId(customerId);
//        personalBase.setOwnerName(customerName);
        personalBase.setPhone(request.getPhone());
        personalBase.setRemark(request.getRemark());
        personalBase.setPersonalType("01");
        personalBaseMapper.insert(personalBase);
        PersonalBaseExt personalBaseExt = new PersonalBaseExt();
        personalBaseExt.setCreateTime(nowDate);
        personalBaseExt.setUpdateTime(nowDate);
        personalBaseExt.setEmail(request.getEmail());
        personalBaseExt.setPersonalId(personalId);
        personalBaseExt.setPersonalLevel(request.getPersonalLevel());
        personalBaseExtMapper.insert(personalBaseExt);
        return BaseApiResponse.newSuccess(Boolean.TRUE);
    }


    public BaseApiResponse unBindWechat(WechatUnBindRequest request) {
        customerBaseAuthMapper.deleteByPersonalId(request.getPersonalId());
        return BaseApiResponse.newSuccess(Boolean.TRUE);
    }

    public BaseApiResponse<List<String>> getPersonalLevelList() {
        Map<String, Object> param = new HashMap<>();
        param.put("type", "personal_level");
        BaseApiResponse<List<DictResponse>> result = adminUserServiceClient.getDictByType(param);
        if(!result.getSuccess()){
            return BaseApiResponse.newFailure(result.getCode(), result.getMessage());
        }
        List<DictResponse> dictResponseList = result.getData();
        List<String> respone = new ArrayList<>();
        if(!CollectionUtils.isEmpty(dictResponseList)){
            for (DictResponse dictResponse : dictResponseList ) {
                respone.add(dictResponse.getCode());
            }
        }
        return BaseApiResponse.newSuccess(respone);
    }

    public BaseApiResponse modifyPersonal(PersonalBaseModifyRequest request, String customerId, String customerName) {
        PersonalBase personalBase = personalBaseMapper.queryDetailByPersonalId(request.getPersonalId());
        Date nowDate =  new Date();
        if(personalBase != null){
            personalBase.setRealName(request.getRealName());
            personalBase.setSex(request.getSex());
            personalBase.setPhone(request.getPhone());
            personalBase.setRemark(request.getRemark());
            personalBase.setUpdateTime(nowDate);
            personalBaseMapper.updateByPrimaryKey(personalBase);
        }
        PersonalBaseExt personalBaseExt = personalBaseExtMapper.queryExtDetailByPersonalId(request.getPersonalId());
        if(personalBaseExt != null){
            personalBaseExt.setUpdateTime(nowDate);
            personalBaseExt.setEmail(request.getEmail());
            personalBaseExt.setPersonalLevel(request.getPersonalLevel());
            personalBaseExtMapper.updateByPrimaryKey(personalBaseExt);
        }
        return BaseApiResponse.newSuccess(Boolean.TRUE);
    }
}