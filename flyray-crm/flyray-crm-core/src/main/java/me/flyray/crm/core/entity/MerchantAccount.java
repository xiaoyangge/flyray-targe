package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import me.flyray.common.entity.BaseEntity;


/**
 * 商户账户信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */

@Data
@Table(name = "merchant_account")
public class MerchantAccount extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    @Id
    private Integer id;
	
	    //账户编号
    @Column(name = "account_id")
    private String accountId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;

	//客户编号
	@Column(name = "customer_id")
	private String customerId;
	
	    //商户编号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //商户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @Column(name = "merchant_type")
    private String merchantType;
	
	    //账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户，ACC010：火源账户，ACC011：火钻账户，ACC012：原力值账户，ACC013：可提现余额账户等......
    @Column(name = "account_type")
    private String accountType;
	
	    //币种  CNY：人民币
    @Column(name = "ccy")
    private String ccy;
	
	    //账户余额
    @Column(name = "account_balance")
    private BigDecimal accountBalance;
	
	    //冻结金额
    @Column(name = "freeze_balance")
    private BigDecimal freezeBalance;
	
	    //校验码（余额加密值）
    @Column(name = "check_sum")
    private String checkSum;
	
	    //账户状态 00：正常，01：冻结
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;

}
