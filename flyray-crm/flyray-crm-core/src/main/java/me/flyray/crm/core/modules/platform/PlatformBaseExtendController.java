package me.flyray.crm.core.modules.platform;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.platform.PlatformBaseExtendBiz;
import me.flyray.crm.core.entity.PlatformBaseExtend;
import me.flyray.crm.facade.request.PlatformBaseExtendRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("platformBaseExtend")
public class PlatformBaseExtendController extends BaseController<PlatformBaseExtendBiz, PlatformBaseExtend> {
	
	@Autowired
	private PlatformBaseExtendBiz platformBaseExtendBiz;
	/**
	 * 添加平台拓展信息
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<PlatformBaseExtend> addPlatformBaseExtend(@RequestBody PlatformBaseExtend entity) throws Exception {
		
		platformBaseExtendBiz.addPlatformBaseExtend(entity);
        return ResponseEntity.ok(entity);
    }
	/**
	 * 接收图片文件
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> upload(HttpServletRequest request) {
		return platformBaseExtendBiz.upload(request);
	}
	/**
	 * 详情
	 */
	@RequestMapping(value = "/queryInfo/{platformId}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> queryInfo(@PathVariable String platformId) {
		return platformBaseExtendBiz.queryInfo(platformId);
	}
	/**
	 * 修改
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public void update(@RequestBody PlatformBaseExtendRequest entity) {
		platformBaseExtendBiz.updatePlatformBase(entity);
	}
}