package me.flyray.crm.core.biz.personal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PersonalDistributionRelation;
import me.flyray.crm.core.mapper.PersonalDistributionRelationMapper;
import me.flyray.crm.facade.request.DistributionRelationQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 个人分销关系
 * @email ${email}
 * @date 2018-07-16 10:15:49
 * http://blog.csdn.net/cctcc/article/details/53992215
 */
@Service
@Slf4j
public class PersonalDistributionRelationBiz extends BaseBiz<PersonalDistributionRelationMapper, PersonalDistributionRelation> {

	@Autowired
	private PersonalDistributionRelationMapper personalDistributionRelationMapper;

	/**
	 * 列表
	 */
	public Map<String, Object> distributionRelations(DistributionRelationQueryRequest param) {
		log.info("查询分享关系，请求参数。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", param.getPlatformId());
		reqMap.put("customerId", param.getCustomerId());
		try {
			PersonalDistributionRelation object = mapper.queryByPersonalId((String) param.getCustomerId());
			respMap.put("code", BizResponseCode.OK.getCode());
			respMap.put("message", BizResponseCode.OK.getMessage());
			respMap.put("personalDistributionRelation", object);
		} catch (Exception e) {
			log.info("查询分享关系, 异常。。。"+e.getMessage());
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			return respMap;
		}
		return respMap;
	}

	public List<PersonalDistributionRelation> queryByPersonalId(String personalId){
//		return personalDistributionRelationMapper.queryByPersonalId(personalId);
		return null;
	}

}