package me.flyray.crm.core.modules.merchant;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.crm.core.entity.MerchantAccount;
import me.flyray.crm.facade.request.MerchantAccountRequest;
import me.flyray.crm.facade.request.MerchantOneAccountRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.merchant.MerchantAccountBiz;

@RestController
@RequestMapping("merchantAccount")
public class MerchantAccountController extends BaseController<MerchantAccountBiz, MerchantAccount> {
	/**
	 * 查询商户账户列表
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午4:19:06
	 * @param platformAccoutConfigRequest
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> pageList(@RequestBody @Valid MerchantAccountRequest merchantAccountRequest){
		Map<String, Object> respMap = baseBiz.pageList(merchantAccountRequest);
        return respMap;
    }
	
	/**
	 * 查询某一商户某一账户余额
	 * @author he
	 * @return
	 */
	@RequestMapping(value = "/queryOneAccount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryOneAccount(@RequestBody @Valid MerchantOneAccountRequest merchantOneAccountRequest){
		Map<String, Object> respMap = baseBiz.queryOneAccount(merchantOneAccountRequest);
        return respMap;
    }
}