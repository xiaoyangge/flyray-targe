package me.flyray.auth.service;


import me.flyray.auth.common.vo.JwtAuthenticationRequest;
import me.flyray.auth.common.vo.JwtAuthenticationResponse;

/**
 * @author bolei
 * 运营后台和业务系统登陆处理
 */

public interface AuthService {

    /**
     * 运营后台统一登陆处理
     * 请求admin
     * @param authenticationRequest
     * @return
     * @throws Exception
     */
    JwtAuthenticationResponse getAdminUserToken(JwtAuthenticationRequest authenticationRequest) throws Exception;

    /**
     * 业务后台登陆处理
     * 请求crm或自己的用户系统
     * @param authenticationRequest
     * @return
     * @throws Exception
     */
    JwtAuthenticationResponse getBizUserToken(JwtAuthenticationRequest authenticationRequest) throws Exception;

    /**
     * token 刷新
     * @param oldToken
     * @return
     * @throws Exception
     */
    String refresh(String oldToken) throws Exception;

    /**
     * token验证
     * @param token
     * @throws Exception
     */
    void validate(String token) throws Exception;

}
