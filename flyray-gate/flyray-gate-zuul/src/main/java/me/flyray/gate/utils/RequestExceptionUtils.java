package me.flyray.gate.utils;

import javax.servlet.http.HttpServletResponse;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.util.EntityUtils;
import com.netflix.zuul.context.RequestContext;

/**
 * @author Mu
 * @Description
 * 网关过滤器异常统一处理
 * */
public class RequestExceptionUtils {
	/**
	 * 网关抛异常
	 * @param body
	 * @param code
	 */
	public static void setFailedRequest(String code, String body) {
		BaseApiResponse baseResponse= new BaseApiResponse(code,body);
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletResponse httpResponse=ctx.getResponse();
		httpResponse.setCharacterEncoding("utf-8");
		ctx.setResponseStatusCode(200);
		if (ctx.getResponseBody() == null) {
			ctx.setResponseBody(EntityUtils.BeanToJsonStr(baseResponse));
			ctx.setSendZuulResponse(false);
		}
	}
}
