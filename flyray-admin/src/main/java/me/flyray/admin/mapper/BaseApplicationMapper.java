package me.flyray.admin.mapper;

import me.flyray.admin.entity.BaseApplication;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author admin
 * @email ${email}
 * @date 2019-03-05 19:50:36
 */
@org.apache.ibatis.annotations.Mapper
public interface BaseApplicationMapper extends Mapper<BaseApplication> {
	
}
