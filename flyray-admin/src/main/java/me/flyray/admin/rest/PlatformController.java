package me.flyray.admin.rest;

import me.flyray.admin.entity.Dept;
import me.flyray.admin.entity.Platform;
import me.flyray.common.util.SnowFlake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.admin.biz.PlatformBiz;
import me.flyray.admin.rpc.service.DeptService;
import me.flyray.admin.rpc.service.PlatformService;
import me.flyray.common.rest.BaseController;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/

@RestController
@RequestMapping("platform")
public class PlatformController extends BaseController<PlatformBiz, Platform> {
	
	@Autowired
    private PlatformService platformService;
	@Autowired
    private DeptService deptService;
	
	/**
	 * 新增一个平台记录的同时初始化该平台的顶层组织结构
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addPlatform(@RequestBody Platform entity) throws Exception {
		long platformId = SnowFlake.getId();
		String platformIdStr = String.valueOf(platformId);
		entity.setPlatformId(platformIdStr);
		platformService.addPlatform(entity);
		Dept dept = new Dept();
		dept.setName(entity.getPlatformName());
		dept.setParentId("-1");
		dept.setPlatformId(platformIdStr);
		deptService.addDept(dept);
        return ResponseEntity.ok(entity);
    }
	
	/**
	 * 
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> updatePlatform(@RequestBody Platform entity) throws Exception {
		platformService.updatePlatform(entity);
		Dept dept = new Dept();
		dept.setName(entity.getPlatformName());
		dept.setPlatformId(entity.getPlatformId());
		deptService.updateByPlatformId(dept);
        return ResponseEntity.ok(entity);
    }
}
