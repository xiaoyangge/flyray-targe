package me.flyray.admin.rest;

import lombok.extern.slf4j.Slf4j;
import me.flyray.admin.biz.BaseApplicationBiz;
import me.flyray.admin.entity.BaseApplication;
import me.flyray.admin.vo.MenuTree;
import me.flyray.common.rest.BaseController;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: bolei
 * @date: 19:17 2019/3/5
 * @Description: 接入的应用
 */

@Slf4j
@RestController
@RequestMapping("app")
public class ApplicationController extends BaseController<BaseApplicationBiz, BaseApplication> {

    @RequestMapping(value = "/role/authority", method = RequestMethod.GET)
    @ResponseBody
    public List<MenuTree> listUserAuthorityMenu(@RequestParam Map<String, Object> param){

        /*String userId = userBiz.getUserByUsername(getCurrentUserName()).getUserId();
        try {
            if (parentId == null) {
                parentId = this.getSystem().get(0).getId();
            }
        } catch (Exception e) {
            return new ArrayList<MenuTree>();
        }
        return getMenuTree(baseBiz.getUserAuthorityMenuByUserId(userId),parentId);*/
        return null;
    }




}
