package me.flyray.admin.vo;

/**
 * 修改用户密码
 * 
 * @author centerroot
 * @time 创建时间:2018年8月27日下午2:59:35
 * @description
 */

public class UserPhone {
	
	private String userId;
	private String phone;
	private String newPhone;
	private String code;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNewPhone() {
		return newPhone;
	}

	public void setNewPhone(String newPhone) {
		this.newPhone = newPhone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
