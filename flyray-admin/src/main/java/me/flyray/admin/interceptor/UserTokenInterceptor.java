package me.flyray.admin.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ace on 2017/9/12.
 */
public class UserTokenInterceptor implements RequestInterceptor {
    private Logger logger = LoggerFactory.getLogger(UserTokenInterceptor.class);

    @Override
    public void apply(RequestTemplate requestTemplate) {
        try {
        	requestTemplate.header("Authorization", "AuthorizationAuthorization");
            requestTemplate.header("x-client-token", "AuthorizationAuthorization");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
