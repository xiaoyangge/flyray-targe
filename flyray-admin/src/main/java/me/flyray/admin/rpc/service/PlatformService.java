package me.flyray.admin.rpc.service;

import me.flyray.admin.entity.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.admin.biz.PlatformBiz;

/** 
* @author: bolei
* @date：2018年4月16日 下午2:17:30 
* @description：类说明
*/

@Service
public class PlatformService {

	@Autowired
    private PlatformBiz platformBiz;
	
	/**
	 * 新增平台记录
	 * @param entity
	 */
	public int addPlatform(Platform entity) {
		return platformBiz.insertSelective(entity);
    }

	public void updatePlatform(Platform entity) {
		platformBiz.updateSelectiveById(entity);
	}
	
}
