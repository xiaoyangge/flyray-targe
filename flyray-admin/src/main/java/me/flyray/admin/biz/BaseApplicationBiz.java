package me.flyray.admin.biz;

import org.springframework.stereotype.Service;

import me.flyray.admin.entity.BaseApplication;
import me.flyray.admin.mapper.BaseApplicationMapper;
import me.flyray.common.biz.BaseBiz;

/**
 * 
 *
 * @author admin
 * @email ${email}
 * @date 2019-03-05 19:50:36
 */
@Service
public class BaseApplicationBiz extends BaseBiz<BaseApplicationMapper,BaseApplication> {
}